import { Component, OnInit } from '@angular/core';
import { IssuesInterface } from 'src/app/shared/issuesInterface';
import { IssuesService } from '../issues.service';

@Component({
  selector: 'app-resolved-issues',
  templateUrl: './resolved-issues.component.html',
  styleUrls: ['./resolved-issues.component.scss']
})
export class ResolvedIssuesComponent implements OnInit {

  resolvedIssues: IssuesInterface[] = [];

  constructor(    
    private issuesService: IssuesService,
    ) { }

  ngOnInit(): void {
    this.loadResolvedIssues();
  }

  private loadResolvedIssues(): void {
    this.issuesService
    .getResolvedIssues()
    .subscribe((resolvedIssues: IssuesInterface[]) => this.resolvedIssues = resolvedIssues);
  }

  removeResolvedIssue(id: number): void {
    this.issuesService.removeResolvedIssue(id)
    .subscribe(() => this.loadResolvedIssues);
  }

  onNameChange(val) {
    
  }
}
