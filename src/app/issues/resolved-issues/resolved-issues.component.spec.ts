import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolvedIssuesComponent } from './resolved-issues.component';

describe('ResolvedIssuesComponent', () => {
  let component: ResolvedIssuesComponent;
  let fixture: ComponentFixture<ResolvedIssuesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResolvedIssuesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolvedIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
