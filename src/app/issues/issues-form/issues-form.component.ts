import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IssuesInterface } from 'src/app/shared/issuesInterface';
import { IssuesService } from '../issues.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-issues-form',
  templateUrl: './issues-form.component.html',
  styleUrls: ['./issues-form.component.scss']
})
export class IssuesFormComponent implements OnInit {

  public issues: IssuesInterface[] = [];
  public issueForm?: FormGroup;
  fieldTextType: boolean;

  constructor(    
    private issuesService: IssuesService,
    private formBuilder: FormBuilder,
    private route: Router
) { }

  ngOnInit(): void {
    this.issueForm = this.createIssueForm();
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  private createIssueForm(): FormGroup {
    return this.formBuilder.group({
      title: ["", Validators.required],
      description: ["", Validators.required],
      contact: ["", Validators.required],
      deadline: ["", Validators.required]
    });
  }

 public addIssue(): void {
    this.issuesService.addIssueService(this.issueForm.value);
  }

  login(): void {
    const name = <HTMLInputElement> document.getElementById("name");    
    const password = <HTMLInputElement> document.getElementById("password");    
    if(name.value === "admin" && password.value === "admin"){
          this.route.navigate(['/issues-list']);
    } else {
      alert("Invalid login credentials! No access!")
    }
  }
}
