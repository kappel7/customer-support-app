import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ISSUES } from '../shared/mock-issues';
import { RESOLVED } from '../shared/mock-issues';
import { IssuesInterface } from '../shared/issuesInterface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotesInterface } from '../shared/notesInterface';
import { NOTES } from '../shared/notes';
import { CounterInterface } from '../shared/counterInterface';
import { COUNTER } from '../shared/counter';

@Injectable()
export class IssuesService {
  mockId = 5;
  reslovedId = 0; 

  constructor(
    private http: HttpClient) { }

  getIssues(): Observable<IssuesInterface[]> {
    return of(ISSUES);
  }

  getNotes(): Observable<NotesInterface[]>{
    return of(NOTES);
  }

  getResolvedIssues(): Observable<IssuesInterface[]> {
    return of(RESOLVED);
  }

  getCounter(): Observable<number> {
    console.log(RESOLVED.length);
    return of(RESOLVED.length)
  }



  addIssueService(issue: IssuesInterface): Observable<number> {
   this.mockId++;
   const issueObject: IssuesInterface = {
     id: this.mockId, ...issue
   };
   console.log(issueObject)
   return of(ISSUES.push(issueObject));
  }

  addResolvedIssueService(issue: IssuesInterface): Observable<number>{
    console.log(RESOLVED);
    return of(RESOLVED.push(issue));
  }

  addNotesService(heading: string, description: string): Observable<number>{
    const noteObject: NotesInterface = {
      id: 1,
      title: heading,
      content: description
    };
    console.log(noteObject);
    return of(NOTES.push(noteObject))
  }

  removeIssue(id: number): Observable<IssuesInterface[]> {
    const index: number = ISSUES.findIndex(issue => issue.id === id);
    this.addResolvedIssueService(ISSUES[index]);
    return of(ISSUES.splice(index, 1));
  }

  removeResolvedIssue(id: number): Observable<IssuesInterface[]> {
    const index: number = RESOLVED.findIndex(issue => issue.id === id);
    return of(RESOLVED.splice(index, 1));
  }

}
