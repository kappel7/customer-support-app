import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CounterInterface } from 'src/app/shared/counterInterface';
import { IssuesInterface } from 'src/app/shared/issuesInterface';
import { NotesInterface } from 'src/app/shared/notesInterface';
import { IssuesService } from '../issues.service';
import { LocalStorageService } from '../local-storage.service';


@Component({
  selector: 'app-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.scss']
})
export class IssuesListComponent implements OnInit { 

  issues: IssuesInterface[] = [];
  notes: NotesInterface[] = [];
  issueForm: FormGroup;
  openState: boolean = false;
  counter: number;
  @Input() title: string;
  @Input() content: string;

  constructor(
    private issuesService: IssuesService,
    private localStorageService: LocalStorageService
    ) {}

  ngOnInit(): void {
    this.loadIssues();
    this.loadCounter();    
  }

  private loadIssues(): void {
    this.issuesService
      .getIssues()
      .subscribe((issues: IssuesInterface[]) => this.issues = issues );
  }

  private loadCounter(): void {
    this.issuesService
    .getCounter()
    .subscribe((counter: number) => this.counter = counter);
  }

  deadlineInfo = (deadlineDay: string) => {
    const deadline = new Date(deadlineDay) 
    const currentDate = new Date()
    if (deadline < currentDate) {
      return 'pastDeadline';
    }
  }

  removeIssue(id: number): void {
    this.issuesService.removeIssue(id)
    .subscribe(() => this.loadIssues);
    this.loadCounter();
  }

  onNameChange(val) {
    
  }

}
