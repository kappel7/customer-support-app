import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssuesListComponent } from './issues-list/issues-list.component';
import { IssuesService } from './issues.service';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IssuesFormComponent } from './issues-form/issues-form.component';
import { CoreModule } from '../core/core.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterModule } from '@angular/router';
import { ResolvedIssuesComponent } from './resolved-issues/resolved-issues.component';




@NgModule({
  declarations: [IssuesListComponent, IssuesFormComponent, ResolvedIssuesComponent],
  providers: [IssuesService],
  imports: [CommonModule, SharedModule, ReactiveFormsModule, CoreModule, MatExpansionModule, RouterModule, FormsModule],
  exports: [IssuesListComponent]
  
  
})
export class IssuesModule { }
