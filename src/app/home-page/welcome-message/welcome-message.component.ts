import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome-message',
  templateUrl: './welcome-message.component.html',
  styleUrls: ['./welcome-message.component.scss']
})
export class WelcomeMessageComponent implements OnInit {

  fieldTextType: boolean;

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  login(): void {
    const name = <HTMLInputElement> document.getElementById("name");    
    const password = <HTMLInputElement> document.getElementById("password");    
    if(name.value === "admin" && password.value === "admin"){
          this.route.navigate(['/issues-list']);
    } else {
      alert("Invalid login credentials! No access!")
    }
  }

}
