import { IssuesInterface } from "./issuesInterface";

export const ISSUES: IssuesInterface[] = [
    {
        id: 1,
        title: "Screen won't turn on",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ornare, orci nec ultricies semper, lacus mauris finibus sapien, at vulputate ex nulla eget ex. Aliquam dapibus a nibh quis condimentum. Maecenas euismod pharetra dignissim. Donec commodo purus tellus, non auctor erat fermentum eu. Vestibulum molestie sem eu risus luctus tincidunt. Maecenas aliquet justo non facilisis aliquam. In id gravida ante, ac ullamcorper neque.",
        contact: "somebody@gmail.com",
        deadline: "2021-06-02"
    },
    {
        id: 2,
        title: "Video is frozen when Zoom meeting starts",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ornare, orci nec ultricies semper, lacus mauris finibus sapien, at vulputate ex nulla eget ex. Aliquam dapibus a nibh quis condimentum. Maecenas euismod pharetra dignissim. Donec commodo purus tellus, non auctor erat fermentum eu. Vestibulum molestie sem eu risus luctus tincidunt. Maecenas aliquet justo non facilisis aliquam. In id gravida ante, ac ullamcorper neque.",
        contact: "bob@gmail.com",
        deadline: "2021-12-02"
    },
    {
        id: 3,
        title: "Can't change language on keyboard",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ornare, orci nec ultricies semper, lacus mauris finibus sapien, at vulputate ex nulla eget ex. Aliquam dapibus a nibh quis condimentum. Maecenas euismod pharetra dignissim. Donec commodo purus tellus, non auctor erat fermentum eu. Vestibulum molestie sem eu risus luctus tincidunt. Maecenas aliquet justo non facilisis aliquam. In id gravida ante, ac ullamcorper neque.",
        contact: "mari@gmail.com",
        deadline: "2021-12-02"
    },
    {
        id: 4,
        title: "Mic is not working",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ornare, orci nec ultricies semper, lacus mauris finibus sapien, at vulputate ex nulla eget ex. Aliquam dapibus a nibh quis condimentum. Maecenas euismod pharetra dignissim. Donec commodo purus tellus, non auctor erat fermentum eu. Vestibulum molestie sem eu risus luctus tincidunt. Maecenas aliquet justo non facilisis aliquam. In id gravida ante, ac ullamcorper neque.",
        contact: "sam@gmail.com",
        deadline: "2021-12-02"
    },
    {
        id: 5,
        title: "Can't connect external screen",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ornare, orci nec ultricies semper, lacus mauris finibus sapien, at vulputate ex nulla eget ex. Aliquam dapibus a nibh quis condimentum. Maecenas euismod pharetra dignissim. Donec commodo purus tellus, non auctor erat fermentum eu. Vestibulum molestie sem eu risus luctus tincidunt. Maecenas aliquet justo non facilisis aliquam. In id gravida ante, ac ullamcorper neque.",
        contact: "tim@gmail.com",
        deadline: "2021-12-15"
    },
];

export const RESOLVED: IssuesInterface[] = [];