export interface IssuesInterface {
    id: number;
    title: string;
    description: string;
    contact: string;
    deadline: string;
}