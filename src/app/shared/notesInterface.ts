export interface NotesInterface {
    id: number;
    title: string;
    content: string;
}
