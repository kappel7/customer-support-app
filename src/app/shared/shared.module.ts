import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { PositionUppercasedPipe } from './pipes/position-uppercased.pipe';



@NgModule({
  declarations: [
    HeaderComponent,
    PositionUppercasedPipe
  ],
  exports: [HeaderComponent, PositionUppercasedPipe],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
