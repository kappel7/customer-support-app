import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeMessageComponent } from './home-page/welcome-message/welcome-message.component';
import { IssuesFormComponent } from './issues/issues-form/issues-form.component';
import { IssuesListComponent } from './issues/issues-list/issues-list.component';
import { ResolvedIssuesComponent } from './issues/resolved-issues/resolved-issues.component';
import { UsersListComponent } from './users/users-list/users-list.component';

const routes: Routes = [
  { path: 'home', component: WelcomeMessageComponent },
  { path: 'issues-form', component: IssuesFormComponent },
  { path: 'issues-list', component: IssuesListComponent },
  { path: 'resolved-issues', component: ResolvedIssuesComponent },
  { path: 'users-list', component: UsersListComponent },
  { path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
