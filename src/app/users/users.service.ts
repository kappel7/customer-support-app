import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { UsersInterface } from '../shared/users-interface';

const httpOptions = {
  header: new HttpHeaders({
    'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  usersUrl: string = 'https://jsonplaceholder.typicode.com/users';
  usersLimit = '?_limit=9';

  constructor(
    private http: HttpClient) { }

  getUsers(): Observable<UsersInterface[]> {
    return this.http.get<UsersInterface[]>(`${this.usersUrl}${this.usersLimit}`);

  }

}
