# Customer Support App 

## What this app is about

This is an app created as the final project for SDA's **JavaScript from Scratch** course that focuses on the aspects of front-end development.

You can have a look at the page [here](https://customersupport.netlify.app/)!

## Pages overview:

* A **Home** page from where a user can reach a complaints form page by following the **Submit Issue!** link in the welcome area or by clicking the puzzle-piece shaped icon further below. The admin can "log in" by following a link in the footer area (username: admin, password: admin) and review the list of submitted issues.
* An **Complaints form** page where the user can submit an issue.
* A **Submitted Issues** page (Submit View) where the admin can mark an issue as resolved which also removes it from the list. There is a section on the right for taking notes.
* A **Resolved Issues** page where the admin can delete an issue permanently. When an issue has reached its deadline, the design of the card will change its colour.
* A **Registered Users** page that displays a list of registered users (from JSONPlaceholder).
